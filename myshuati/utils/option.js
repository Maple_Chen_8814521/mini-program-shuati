  const switchNumberToLetters = number => {
    if (number<0 || number>25){
      return "Number out of bound error"
    }
    let optionKey = [
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
    ]
    return optionKey[number]
  }

  const switchLettersToNumber = letter => {
    let optionKey = [
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
    ]
    for (let index in optionKey){
      if (letter == optionKey[index]){
        return index
      }
    }
    return -1
  }

  /**
   * 冒泡排序：一种排序的稳定算法，效率低于快速排序。
   * 稳定性：若数组中有两个相同的数字2，它不会交换前边的2和后边的2的顺序
   * 结果：调用此方法的结果为，从左到右结果按小到大排序
   * 时间复杂度：方法内第一层的for域只有一个，第一层for域内为双层for循环，空间复杂度理解为O(n^2)，
   * 空间复杂度：临时变量开辟的空间大小，仅有一个temp, O(1)
   * @param {*} arr 
   * @param {*} arrlength 
   */
  const bubbleSort = (arr, arrlength) => {
    var i, j;
    for (i=0; i<arrlength-1; i++) /* 外循环为排序趟数，len个数进行len-1趟 */
      for (j=0; j<arrlength-1-i; j++) { /* 内循环为每趟比较的次数，第i趟比较len-i次 */
        if (arr[j] > arr[j+1]) { /* 相邻元素比较，若逆序则交换（升序为左大于右，降序反之） */
          temp = arr[j];
          arr[j] = arr[j+1];
          arr[j+1] = temp;
        }
      }
    return arr
  }

  /**
   * 快速排序：非稳定算法，效率极高，常被各算法题引用
   * 结果：从左到右结果按小到大排序
   * 时间复杂度：O( nlogn )=O( nlog(10)n )=O( log(10)n^2 )
   * 空间复杂度：O(logn)  
   */
  const quickSort = (arr, low, high) => {
    let pivot = arr[low]; // 取(当前递归深度)排序片段的第一个值作为中轴(或叫做基准值)
    var start = low;
    var end = high;
    // 对当前片段中的数进行初步排序，事实上这个最外层的while只会走一次
    while (start < end){
      // 从后往前比较，找到比中轴值小的数
      while ( start < end && arr[end] >= pivot )
        end--;
      // 当找到第一个比基准值小的数时，交换位置。这时基准值会将数组划分为左右两片区域
      if(arr[end] <= pivot){
        let temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
      }
      // 此时在中轴的左扇区，从前往后开始比较，找到第一个比中轴值大的数。(中轴右边的不做比较)
      while ( start < end && arr[start] <= pivot )
        start++;
      if(arr[start] >= pivot){
        let temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
      }
      // console.log(arr); 每一次的排序结果
    }
    // 到了这里，中轴左边的数都比基准值小，中轴右边的数都比基准值大。且当前start=end。
    /**
     * 下边需要执行递归：
     * (1).若中轴线左边还有扇区，则排序左扇区 (注：中轴线index=start=end不需要被重复计算)
     * (2).若中轴线右边还有扇区，则排序右扇区 (注：中轴线index=start=end不需要被重复计算)
     */
    if (start > low) arr = quickSort(arr, low, start-1);
    if (end < high) arr = quickSort(arr, end+1, high);
    return arr;
  }

  module.exports = {
    switchNumberToLetters: switchNumberToLetters,
    switchLettersToNumber: switchLettersToNumber,
    bubbleSort: bubbleSort,
    quickSort: quickSort
  }
  