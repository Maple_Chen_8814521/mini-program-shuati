const { splitFunc } = require('../../utils/option.js')
const option_util = require('../../utils/option.js')
Page({
    /**
     * 页面的初始数据
     */
    data: {
        question: {},
        answer: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        /**
         * 1.模拟调用后台接口，获得题目集Json
         */
        let obj = {
            "1": {
                "id": "ques_id_01",
                "title": "中华人民共和国成立的年份是？",
                "options": [
                    "1947",
                    "1948",
                    "1949",
                    "1950"
                ],
                "correct_answer": [
                    "1949"
                ]
            },
            "2": {
                "id": "ques_id_02",
                "title": "以下属于日期类型的有？",
                "options": [
                    "1949年",
                    "10月",
                    "08:46:00",
                    "航母"
                ],
                "correct_answer": [
                    "1949年",
                    "10月",
                    "08:46:00"
                ]
            },
            "3": {
                "id": "ques_id_03",
                "title": "以下属于动物的是？",
                "options": [
                    "火箭",
                    "轮船",
                    "水母",
                    "手机"
                ],
                "correct_answer": [
                    "水母"
                ]
            }
        }
        // 2.将获得的json赋值给当前页面Data对象
        let that = this
        that.setData({
            question: obj
        })
        // 3.遍历对象，根据题目总数初始化answer对象，设置view答案域默认隐藏
        let ans = this.data.answer
        for(let key in obj){
            ans[key] = { showSolution: false }
            ans[key]['oprateName'] = '点击查看答案'
            // console.log(obj[key].correct_answer.length)
        }
        that.setData({
            answer: ans
        })
        // 4.使用工具类
        // (1)根据数字索引换取大写字母
        // console.log(option_util.switchNumberToLetters(26-1))
        // (2)冒泡排序
        // let arr = [6, 4, 3, 1, 5]
        // console.log(option_util.bubbleSort(arr, arr.length))
        // (3)快排
        // console.log(option_util.quickSort(arr, 0, arr.length -1));
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 触发单选题答题，仅打印value，可以去掉
     * @param {题目id} question_id 
     */
    radioChoiceChange: function(event){
        console.log('[pls neglect!] checkbox发生change事件，携带value值为：', event.detail.value)
    },

    /**
     * 触发多选题答题，仅打印value，可以去掉
     * @param {题目id} question_id 
     */
    checkboxChoiceChange: function(event){
        console.log('[pls neglect!] checkbox发生change事件，携带value值为：', event.detail.value)
        // 点击题目的选项，选项携带的参数
        let index = event.currentTarget.dataset.num
        // 获取data中的对象
        let ques = this.data.question
        let ans = this.data.answer
        var correct_answer_str = ''  // 定义正确答案的选项
        var your_choice_str = ''  // 定义你之前已经选择的选项
        if (ques[index]['correct_answer'].length>0){
            // 1.判断题目之前是否计算过正确答案，否则进行计算
            if (ans[index]['correct_choice'] == null){
                // 1.题目的正确答案为
                var correct_answer = ques[index]['correct_answer']
                var options = ques[index]['options']
                var correct_answer_array = []
                for(let index in options){
                    for(let undex in correct_answer)
                        if (options[index] == correct_answer[undex])
                            correct_answer_array.push(index)   // 将正确答案的下标放入数组
                }
                for(let index in correct_answer_array){
                    // 拼接正确答案的字符串（上边的ABCD绝对已经排序好的，不会出现如BAC乱序的情况）
                    if (index == 0){
                        correct_answer_str += option_util.switchNumberToLetters(correct_answer_array[index])
                    }else{
                        correct_answer_str += '、' + option_util.switchNumberToLetters(correct_answer_array[index])
                    }
                }
            }else{
                correct_answer_str = ans[index]['correct_choice']
            }
            // 2.你的选项
            var selection_unordered = event.detail.value
            var selection_order = []
            for(let index in selection_unordered)
                selection_order.push(option_util.switchLettersToNumber(selection_unordered[index]))
            // 使用冒泡排序，规整ABCD顺序
            selection_order = option_util.bubbleSort(selection_order, selection_order.length)
            // 将你的选项化成字符串
            for(let index in selection_order){
                // 拼接正确答案的字符串（上边的ABCD绝对已经排序好的，不会出现如BAC乱序的情况）
                if (index == 0){
                    your_choice_str += option_util.switchNumberToLetters(selection_order[index])
                }else{
                    your_choice_str += '、' + option_util.switchNumberToLetters(selection_order[index])
                }
            }
        }
        // 更新data区域的answer对象
        ans[index]['your_choice'] = your_choice_str
        ans[index]['correct_choice'] = correct_answer_str
        ans[index]['sentence_result'] = correct_answer_str==your_choice_str?'正确':'不正确'
        this.setData({
            answer: ans
        })
    },

    /**
     * 使用bindtap绑定此函数，同时声明多个变量，如data-num、data-ab等，即声明num、ab变量
     * 【取代以前使用html开发，都是将数据拼接成字符串放至value,再又后台分割解析的繁杂方法】
     * @param {*} event 
     */
    radioChange(event) {
        // 点击题目的选项，选项携带的参数
        let index = event.currentTarget.dataset.num 
        let choice = event.currentTarget.dataset.chioce  // 你的答案选项
        // 获取data中的对象
        let ques = this.data.question
        if (ques[index]['correct_answer'].length>0){
            // 1.题目的正确答案为
            var correct_answer = ques[index]['correct_answer']
            var correct_answer_str = ''  // 定义正确答案的选项
            var options = ques[index]['options']
            var correct_answer_array = []
            for(let index in options){
                for(let undex in correct_answer)
                    if (options[index] == correct_answer[undex])
                        correct_answer_array.push(index)   // 将正确答案的下标放入数组
            }
            for(let index in correct_answer_array){
                // 拼接正确答案的字符串（上边的ABCD绝对已经排序好的，不会出现如BAC乱序的情况）
                correct_answer_str += option_util.switchNumberToLetters(correct_answer_array[index])
            }
            // 更新data区域的answer对象
            let ans = this.data.answer
            ans[index]['your_choice'] = choice
            ans[index]['correct_choice'] = correct_answer_str
            ans[index]['sentence_result'] = correct_answer_str==choice?'正确':'不正确'
            this.setData({
                answer: ans
            })
        }
    },

    // 展示或隐藏答案,根据id拿到dom节点
    checkAns(event) {
        let id = event.currentTarget.dataset.id
        let ans = this.data.answer
        // 查看是否已经选择答案
        if (ans[id]['correct_choice'] == null){
            wx.showToast({
                title: '请先选择一个选项', // 标题
                icon: 'none',  // 图标类型，默认success
                duration: 1500  // 提示窗停留时间，默认1500ms
            })
        }else{
            // 重置按钮功能
            if (ans[id]['showSolution']) {
                // console.log('is true')
                ans[id]['showSolution'] = false
                ans[id]['oprateName'] = '点击查看答案'
            }else if (!ans[id]['showSolution']) {
                // console.log('is false')
                ans[id]['showSolution'] = true
                ans[id]['oprateName'] = '点击隐藏答案'
            }
            this.setData({
                answer: ans
            })
        }
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})